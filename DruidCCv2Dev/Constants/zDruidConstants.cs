﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DruidCCv2Dev.Constants
{
    public static class zDruidConstants
    {
        public static string ccName = "zDruid";
        public static string ccVersion = "0.1.5";
        public static string ccGUITitle = " [Leveling] Alpha";
    }
}
