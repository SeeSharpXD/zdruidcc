﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DruidCCv2Dev.Constants;
using System.Runtime.InteropServices;

namespace DruidCCv2Dev.GUI
{
    public partial class WelcomeForm : Form
    {

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        public zDruid parentCCObj;
        public WelcomeForm()
        {
            InitializeComponent();
        }

        private void WelcomeForm_Load(object sender, EventArgs e)
        {

        }

        private void WelcomeForm_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void buttonCloseWelcomeWindow_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void buttonConfigureCC_Click(object sender, EventArgs e)
        {
            parentCCObj.ShowGui();
        }

        private void linkLabelSeeSharp_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://zzukbot.com/forum/memberlist.php?mode=viewprofile&u=1675");
        }

        private void linkLabelForumThread_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://zzukbot.com/forum/viewtopic.php?f=61&t=1151");
        }
    }
}
