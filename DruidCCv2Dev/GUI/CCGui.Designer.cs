﻿namespace DruidCCv2Dev.GUI
{
    partial class CCGui
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.labelCCCuiTitle = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label19 = new System.Windows.Forms.Label();
            this.buttonSaveAndCloseGUI = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.numericUpDownRestHealPercent = new System.Windows.Forms.NumericUpDown();
            this.checkBoxRestUseHeal = new System.Windows.Forms.CheckBox();
            this.textBoxRestDrinkname = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxRestFoodname = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBoxPullWithShapeshiftAuto = new System.Windows.Forms.CheckBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.checkBoxNoPullIfTargetSkull = new System.Windows.Forms.CheckBox();
            this.numericUpDownNoPullIfTargerLevelGreater = new System.Windows.Forms.NumericUpDown();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.numericUpDownNoPullIfTargetLevelLess = new System.Windows.Forms.NumericUpDown();
            this.label27 = new System.Windows.Forms.Label();
            this.textBoxPullingSpell = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.numericUpDownPullRange = new System.Windows.Forms.NumericUpDown();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.numericUpDownSwipeMinTargets = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.numericUpDownBearHealHPPercent = new System.Windows.Forms.NumericUpDown();
            this.radioButtonBearRotation = new System.Windows.Forms.RadioButton();
            this.checkBoxBearUseBash = new System.Windows.Forms.CheckBox();
            this.checkBoxBearUseEnrage = new System.Windows.Forms.CheckBox();
            this.checkBoxBearUseMaul = new System.Windows.Forms.CheckBox();
            this.numericUpDownDemoRoarMinTargets = new System.Windows.Forms.NumericUpDown();
            this.checkBoxBearUseSwipe = new System.Windows.Forms.CheckBox();
            this.checkBoxBearUseDemoRoar = new System.Windows.Forms.CheckBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.checkBoxLowbieUseMoonfire = new System.Windows.Forms.CheckBox();
            this.checkBoxLowbieUseWrath = new System.Windows.Forms.CheckBox();
            this.radioButtonBalanceRotation = new System.Windows.Forms.RadioButton();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.radioButtonCatRotation = new System.Windows.Forms.RadioButton();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.checkBoxUseHealingTouch = new System.Windows.Forms.CheckBox();
            this.checkBoxUseRegrowth = new System.Windows.Forms.CheckBox();
            this.checkBoxUseRejuvination = new System.Windows.Forms.CheckBox();
            this.label11 = new System.Windows.Forms.Label();
            this.numericUpDownHealHealingTouchHP = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.numericUpDownRegrowHP = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.numericUpDownHealRejivHp = new System.Windows.Forms.NumericUpDown();
            this.checkBoxHealCancelShapeshift = new System.Windows.Forms.CheckBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.linkLabelEmu = new System.Windows.Forms.LinkLabel();
            this.linkLabelZzuk = new System.Windows.Forms.LinkLabel();
            this.buttonAboutForumThread = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.linkLabelSeeSharp = new System.Windows.Forms.LinkLabel();
            this.labelCCVersionName = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.checkBoxBuffThorns = new System.Windows.Forms.CheckBox();
            this.checkBoxBuffMarkOfTheWild = new System.Windows.Forms.CheckBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.label23 = new System.Windows.Forms.Label();
            this.buttonWelcomeMessageShow = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.buttonLoadDefaultSettings = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.label13 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRestHealPercent)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownNoPullIfTargerLevelGreater)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownNoPullIfTargetLevelLess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPullRange)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSwipeMinTargets)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBearHealHPPercent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDemoRoarMinTargets)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHealHealingTouchHP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRegrowHP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHealRejivHp)).BeginInit();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.groupBox8.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DimGray;
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Controls.Add(this.labelCCCuiTitle);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Location = new System.Drawing.Point(-1, -1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(866, 30);
            this.panel1.TabIndex = 0;
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::DruidCCv2Dev.Properties.Resources.blueMinus;
            this.pictureBox2.Location = new System.Drawing.Point(794, 3);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(24, 24);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 2;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // labelCCCuiTitle
            // 
            this.labelCCCuiTitle.AutoSize = true;
            this.labelCCCuiTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCCCuiTitle.Location = new System.Drawing.Point(4, 7);
            this.labelCCCuiTitle.Name = "labelCCCuiTitle";
            this.labelCCCuiTitle.Size = new System.Drawing.Size(102, 18);
            this.labelCCCuiTitle.TabIndex = 1;
            this.labelCCCuiTitle.Text = "zDruid Label";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::DruidCCv2Dev.Properties.Resources.redx;
            this.pictureBox1.Location = new System.Drawing.Point(824, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(24, 24);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(16)))), ((int)(((byte)(16)))));
            this.panel2.Controls.Add(this.label19);
            this.panel2.Controls.Add(this.buttonSaveAndCloseGUI);
            this.panel2.Location = new System.Drawing.Point(-1, 546);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(866, 47);
            this.panel2.TabIndex = 1;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label19.Location = new System.Drawing.Point(427, 15);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(171, 16);
            this.label19.TabIndex = 7;
            this.label19.Text = "Click when you\'re done ---->";
            // 
            // buttonSaveAndCloseGUI
            // 
            this.buttonSaveAndCloseGUI.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSaveAndCloseGUI.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.buttonSaveAndCloseGUI.Location = new System.Drawing.Point(606, 5);
            this.buttonSaveAndCloseGUI.Name = "buttonSaveAndCloseGUI";
            this.buttonSaveAndCloseGUI.Size = new System.Drawing.Size(243, 35);
            this.buttonSaveAndCloseGUI.TabIndex = 0;
            this.buttonSaveAndCloseGUI.Text = "Save Settings and Close GUI";
            this.buttonSaveAndCloseGUI.UseVisualStyleBackColor = true;
            this.buttonSaveAndCloseGUI.Click += new System.EventHandler(this.buttonSaveAndCloseGUI_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.numericUpDownRestHealPercent);
            this.groupBox1.Controls.Add(this.checkBoxRestUseHeal);
            this.groupBox1.Controls.Add(this.textBoxRestDrinkname);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.textBoxRestFoodname);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.groupBox1.Location = new System.Drawing.Point(5, 412);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(206, 128);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Rest Settings";
            // 
            // numericUpDownRestHealPercent
            // 
            this.numericUpDownRestHealPercent.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownRestHealPercent.Location = new System.Drawing.Point(135, 91);
            this.numericUpDownRestHealPercent.Maximum = new decimal(new int[] {
            95,
            0,
            0,
            0});
            this.numericUpDownRestHealPercent.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownRestHealPercent.Name = "numericUpDownRestHealPercent";
            this.numericUpDownRestHealPercent.Size = new System.Drawing.Size(54, 22);
            this.numericUpDownRestHealPercent.TabIndex = 45;
            this.numericUpDownRestHealPercent.Value = new decimal(new int[] {
            25,
            0,
            0,
            0});
            // 
            // checkBoxRestUseHeal
            // 
            this.checkBoxRestUseHeal.AutoSize = true;
            this.checkBoxRestUseHeal.Checked = true;
            this.checkBoxRestUseHeal.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxRestUseHeal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxRestUseHeal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBoxRestUseHeal.Location = new System.Drawing.Point(10, 93);
            this.checkBoxRestUseHeal.Name = "checkBoxRestUseHeal";
            this.checkBoxRestUseHeal.Size = new System.Drawing.Size(119, 20);
            this.checkBoxRestUseHeal.TabIndex = 42;
            this.checkBoxRestUseHeal.Text = "Heal if HP % <=";
            this.checkBoxRestUseHeal.UseVisualStyleBackColor = true;
            // 
            // textBoxRestDrinkname
            // 
            this.textBoxRestDrinkname.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxRestDrinkname.Location = new System.Drawing.Point(49, 63);
            this.textBoxRestDrinkname.Name = "textBoxRestDrinkname";
            this.textBoxRestDrinkname.Size = new System.Drawing.Size(140, 22);
            this.textBoxRestDrinkname.TabIndex = 39;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label9.Location = new System.Drawing.Point(7, 66);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(42, 16);
            this.label9.TabIndex = 38;
            this.label9.Text = "Drink:";
            // 
            // textBoxRestFoodname
            // 
            this.textBoxRestFoodname.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxRestFoodname.Location = new System.Drawing.Point(49, 32);
            this.textBoxRestFoodname.Name = "textBoxRestFoodname";
            this.textBoxRestFoodname.Size = new System.Drawing.Size(140, 22);
            this.textBoxRestFoodname.TabIndex = 37;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label5.Location = new System.Drawing.Point(6, 35);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 16);
            this.label5.TabIndex = 36;
            this.label5.Text = "Food:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.checkBox1);
            this.groupBox2.Controls.Add(this.checkBoxPullWithShapeshiftAuto);
            this.groupBox2.Controls.Add(this.panel3);
            this.groupBox2.Controls.Add(this.label27);
            this.groupBox2.Controls.Add(this.textBoxPullingSpell);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.numericUpDownPullRange);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.groupBox2.Location = new System.Drawing.Point(214, 274);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(206, 266);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Pull Settings";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBox1.Location = new System.Drawing.Point(10, 111);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(152, 20);
            this.checkBox1.TabIndex = 42;
            this.checkBox1.Text = "Fairy Fire if possable";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // checkBoxPullWithShapeshiftAuto
            // 
            this.checkBoxPullWithShapeshiftAuto.AutoSize = true;
            this.checkBoxPullWithShapeshiftAuto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxPullWithShapeshiftAuto.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBoxPullWithShapeshiftAuto.Location = new System.Drawing.Point(10, 85);
            this.checkBoxPullWithShapeshiftAuto.Name = "checkBoxPullWithShapeshiftAuto";
            this.checkBoxPullWithShapeshiftAuto.Size = new System.Drawing.Size(185, 20);
            this.checkBoxPullWithShapeshiftAuto.TabIndex = 41;
            this.checkBoxPullWithShapeshiftAuto.Text = "Shapeshift Auto Attack Pull";
            this.checkBoxPullWithShapeshiftAuto.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.panel3.Controls.Add(this.checkBoxNoPullIfTargetSkull);
            this.panel3.Controls.Add(this.numericUpDownNoPullIfTargerLevelGreater);
            this.panel3.Controls.Add(this.label29);
            this.panel3.Controls.Add(this.label28);
            this.panel3.Controls.Add(this.numericUpDownNoPullIfTargetLevelLess);
            this.panel3.Location = new System.Drawing.Point(9, 163);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(187, 95);
            this.panel3.TabIndex = 40;
            // 
            // checkBoxNoPullIfTargetSkull
            // 
            this.checkBoxNoPullIfTargetSkull.AutoSize = true;
            this.checkBoxNoPullIfTargetSkull.Checked = true;
            this.checkBoxNoPullIfTargetSkull.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxNoPullIfTargetSkull.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxNoPullIfTargetSkull.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBoxNoPullIfTargetSkull.Location = new System.Drawing.Point(12, 6);
            this.checkBoxNoPullIfTargetSkull.Name = "checkBoxNoPullIfTargetSkull";
            this.checkBoxNoPullIfTargetSkull.Size = new System.Drawing.Size(137, 20);
            this.checkBoxNoPullIfTargetSkull.TabIndex = 36;
            this.checkBoxNoPullIfTargetSkull.Text = "Target is ?? (Skull)";
            this.checkBoxNoPullIfTargetSkull.UseVisualStyleBackColor = true;
            // 
            // numericUpDownNoPullIfTargerLevelGreater
            // 
            this.numericUpDownNoPullIfTargerLevelGreater.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownNoPullIfTargerLevelGreater.Location = new System.Drawing.Point(132, 30);
            this.numericUpDownNoPullIfTargerLevelGreater.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownNoPullIfTargerLevelGreater.Minimum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.numericUpDownNoPullIfTargerLevelGreater.Name = "numericUpDownNoPullIfTargerLevelGreater";
            this.numericUpDownNoPullIfTargerLevelGreater.Size = new System.Drawing.Size(42, 22);
            this.numericUpDownNoPullIfTargerLevelGreater.TabIndex = 26;
            this.numericUpDownNoPullIfTargerLevelGreater.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label29.Location = new System.Drawing.Point(3, 61);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(125, 16);
            this.label29.TabIndex = 29;
            this.label29.Text = "Tar lvl is < yours by:";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label28.Location = new System.Drawing.Point(3, 34);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(125, 16);
            this.label28.TabIndex = 27;
            this.label28.Text = "Tar lvl is > yours by:";
            // 
            // numericUpDownNoPullIfTargetLevelLess
            // 
            this.numericUpDownNoPullIfTargetLevelLess.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownNoPullIfTargetLevelLess.Location = new System.Drawing.Point(132, 58);
            this.numericUpDownNoPullIfTargetLevelLess.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.numericUpDownNoPullIfTargetLevelLess.Minimum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.numericUpDownNoPullIfTargetLevelLess.Name = "numericUpDownNoPullIfTargetLevelLess";
            this.numericUpDownNoPullIfTargetLevelLess.Size = new System.Drawing.Size(42, 22);
            this.numericUpDownNoPullIfTargetLevelLess.TabIndex = 28;
            this.numericUpDownNoPullIfTargetLevelLess.Value = new decimal(new int[] {
            59,
            0,
            0,
            0});
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label27.Location = new System.Drawing.Point(15, 144);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(125, 16);
            this.label27.TabIndex = 39;
            this.label27.Text = "Dont Pull if: (NYI)";
            // 
            // textBoxPullingSpell
            // 
            this.textBoxPullingSpell.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPullingSpell.Location = new System.Drawing.Point(76, 57);
            this.textBoxPullingSpell.Name = "textBoxPullingSpell";
            this.textBoxPullingSpell.Size = new System.Drawing.Size(112, 22);
            this.textBoxPullingSpell.TabIndex = 27;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label14.Location = new System.Drawing.Point(7, 60);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(67, 16);
            this.label14.TabIndex = 26;
            this.label14.Text = "Pull Spell:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label12.Location = new System.Drawing.Point(7, 33);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(77, 16);
            this.label12.TabIndex = 24;
            this.label12.Text = "Pull Range:";
            // 
            // numericUpDownPullRange
            // 
            this.numericUpDownPullRange.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownPullRange.Location = new System.Drawing.Point(87, 31);
            this.numericUpDownPullRange.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.numericUpDownPullRange.Minimum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.numericUpDownPullRange.Name = "numericUpDownPullRange";
            this.numericUpDownPullRange.Size = new System.Drawing.Size(58, 22);
            this.numericUpDownPullRange.TabIndex = 25;
            this.numericUpDownPullRange.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.numericUpDownSwipeMinTargets);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.numericUpDownBearHealHPPercent);
            this.groupBox3.Controls.Add(this.radioButtonBearRotation);
            this.groupBox3.Controls.Add(this.checkBoxBearUseBash);
            this.groupBox3.Controls.Add(this.checkBoxBearUseEnrage);
            this.groupBox3.Controls.Add(this.checkBoxBearUseMaul);
            this.groupBox3.Controls.Add(this.numericUpDownDemoRoarMinTargets);
            this.groupBox3.Controls.Add(this.checkBoxBearUseSwipe);
            this.groupBox3.Controls.Add(this.checkBoxBearUseDemoRoar);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.groupBox3.Location = new System.Drawing.Point(429, 35);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(206, 275);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Bear Settings";
            // 
            // numericUpDownSwipeMinTargets
            // 
            this.numericUpDownSwipeMinTargets.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownSwipeMinTargets.Location = new System.Drawing.Point(128, 97);
            this.numericUpDownSwipeMinTargets.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownSwipeMinTargets.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownSwipeMinTargets.Name = "numericUpDownSwipeMinTargets";
            this.numericUpDownSwipeMinTargets.Size = new System.Drawing.Size(40, 22);
            this.numericUpDownSwipeMinTargets.TabIndex = 61;
            this.numericUpDownSwipeMinTargets.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label3.Location = new System.Drawing.Point(11, 228);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 16);
            this.label3.TabIndex = 60;
            this.label3.Text = "Heal if HP% <=";
            // 
            // numericUpDownBearHealHPPercent
            // 
            this.numericUpDownBearHealHPPercent.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownBearHealHPPercent.Location = new System.Drawing.Point(114, 226);
            this.numericUpDownBearHealHPPercent.Maximum = new decimal(new int[] {
            85,
            0,
            0,
            0});
            this.numericUpDownBearHealHPPercent.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownBearHealHPPercent.Name = "numericUpDownBearHealHPPercent";
            this.numericUpDownBearHealHPPercent.Size = new System.Drawing.Size(42, 22);
            this.numericUpDownBearHealHPPercent.TabIndex = 59;
            this.numericUpDownBearHealHPPercent.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // radioButtonBearRotation
            // 
            this.radioButtonBearRotation.AutoSize = true;
            this.radioButtonBearRotation.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.radioButtonBearRotation.Location = new System.Drawing.Point(12, 23);
            this.radioButtonBearRotation.Name = "radioButtonBearRotation";
            this.radioButtonBearRotation.Size = new System.Drawing.Size(165, 22);
            this.radioButtonBearRotation.TabIndex = 50;
            this.radioButtonBearRotation.TabStop = true;
            this.radioButtonBearRotation.Text = "Use Bear Rotation";
            this.radioButtonBearRotation.UseVisualStyleBackColor = true;
            this.radioButtonBearRotation.Click += new System.EventHandler(this.radioButtonBearRotation_Click);
            // 
            // checkBoxBearUseBash
            // 
            this.checkBoxBearUseBash.AutoSize = true;
            this.checkBoxBearUseBash.Checked = true;
            this.checkBoxBearUseBash.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxBearUseBash.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxBearUseBash.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBoxBearUseBash.Location = new System.Drawing.Point(12, 157);
            this.checkBoxBearUseBash.Name = "checkBoxBearUseBash";
            this.checkBoxBearUseBash.Size = new System.Drawing.Size(96, 20);
            this.checkBoxBearUseBash.TabIndex = 49;
            this.checkBoxBearUseBash.Text = "Smart Bash";
            this.checkBoxBearUseBash.UseVisualStyleBackColor = true;
            // 
            // checkBoxBearUseEnrage
            // 
            this.checkBoxBearUseEnrage.AutoSize = true;
            this.checkBoxBearUseEnrage.Checked = true;
            this.checkBoxBearUseEnrage.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxBearUseEnrage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxBearUseEnrage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBoxBearUseEnrage.Location = new System.Drawing.Point(12, 131);
            this.checkBoxBearUseEnrage.Name = "checkBoxBearUseEnrage";
            this.checkBoxBearUseEnrage.Size = new System.Drawing.Size(109, 20);
            this.checkBoxBearUseEnrage.TabIndex = 48;
            this.checkBoxBearUseEnrage.Text = "Smart Enrage";
            this.checkBoxBearUseEnrage.UseVisualStyleBackColor = true;
            // 
            // checkBoxBearUseMaul
            // 
            this.checkBoxBearUseMaul.AutoSize = true;
            this.checkBoxBearUseMaul.Checked = true;
            this.checkBoxBearUseMaul.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxBearUseMaul.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxBearUseMaul.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBoxBearUseMaul.Location = new System.Drawing.Point(12, 184);
            this.checkBoxBearUseMaul.Name = "checkBoxBearUseMaul";
            this.checkBoxBearUseMaul.Size = new System.Drawing.Size(88, 20);
            this.checkBoxBearUseMaul.TabIndex = 47;
            this.checkBoxBearUseMaul.Text = "Maul Filler";
            this.checkBoxBearUseMaul.UseVisualStyleBackColor = true;
            // 
            // numericUpDownDemoRoarMinTargets
            // 
            this.numericUpDownDemoRoarMinTargets.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownDemoRoarMinTargets.Location = new System.Drawing.Point(160, 62);
            this.numericUpDownDemoRoarMinTargets.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownDemoRoarMinTargets.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownDemoRoarMinTargets.Name = "numericUpDownDemoRoarMinTargets";
            this.numericUpDownDemoRoarMinTargets.Size = new System.Drawing.Size(40, 22);
            this.numericUpDownDemoRoarMinTargets.TabIndex = 46;
            this.numericUpDownDemoRoarMinTargets.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // checkBoxBearUseSwipe
            // 
            this.checkBoxBearUseSwipe.AutoSize = true;
            this.checkBoxBearUseSwipe.Checked = true;
            this.checkBoxBearUseSwipe.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxBearUseSwipe.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxBearUseSwipe.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBoxBearUseSwipe.Location = new System.Drawing.Point(12, 99);
            this.checkBoxBearUseSwipe.Name = "checkBoxBearUseSwipe";
            this.checkBoxBearUseSwipe.Size = new System.Drawing.Size(120, 20);
            this.checkBoxBearUseSwipe.TabIndex = 45;
            this.checkBoxBearUseSwipe.Text = "Swipe if targets:";
            this.checkBoxBearUseSwipe.UseVisualStyleBackColor = true;
            // 
            // checkBoxBearUseDemoRoar
            // 
            this.checkBoxBearUseDemoRoar.AutoSize = true;
            this.checkBoxBearUseDemoRoar.Checked = true;
            this.checkBoxBearUseDemoRoar.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxBearUseDemoRoar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxBearUseDemoRoar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBoxBearUseDemoRoar.Location = new System.Drawing.Point(12, 64);
            this.checkBoxBearUseDemoRoar.Name = "checkBoxBearUseDemoRoar";
            this.checkBoxBearUseDemoRoar.Size = new System.Drawing.Size(153, 20);
            this.checkBoxBearUseDemoRoar.TabIndex = 44;
            this.checkBoxBearUseDemoRoar.Text = "Demo Roar if targets:";
            this.checkBoxBearUseDemoRoar.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.checkBoxLowbieUseMoonfire);
            this.groupBox4.Controls.Add(this.checkBoxLowbieUseWrath);
            this.groupBox4.Controls.Add(this.radioButtonBalanceRotation);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.groupBox4.Location = new System.Drawing.Point(5, 35);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(206, 168);
            this.groupBox4.TabIndex = 5;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Balance Settings";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Gray;
            this.label7.Location = new System.Drawing.Point(28, 128);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(123, 16);
            this.label7.TabIndex = 55;
            this.label7.Text = "More Coming Soon";
            // 
            // checkBoxLowbieUseMoonfire
            // 
            this.checkBoxLowbieUseMoonfire.AutoSize = true;
            this.checkBoxLowbieUseMoonfire.Checked = true;
            this.checkBoxLowbieUseMoonfire.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxLowbieUseMoonfire.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxLowbieUseMoonfire.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBoxLowbieUseMoonfire.Location = new System.Drawing.Point(6, 81);
            this.checkBoxLowbieUseMoonfire.Name = "checkBoxLowbieUseMoonfire";
            this.checkBoxLowbieUseMoonfire.Size = new System.Drawing.Size(107, 20);
            this.checkBoxLowbieUseMoonfire.TabIndex = 54;
            this.checkBoxLowbieUseMoonfire.Text = "Use Moonfire";
            this.checkBoxLowbieUseMoonfire.UseVisualStyleBackColor = true;
            // 
            // checkBoxLowbieUseWrath
            // 
            this.checkBoxLowbieUseWrath.AutoSize = true;
            this.checkBoxLowbieUseWrath.Checked = true;
            this.checkBoxLowbieUseWrath.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxLowbieUseWrath.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxLowbieUseWrath.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBoxLowbieUseWrath.Location = new System.Drawing.Point(6, 55);
            this.checkBoxLowbieUseWrath.Name = "checkBoxLowbieUseWrath";
            this.checkBoxLowbieUseWrath.Size = new System.Drawing.Size(90, 20);
            this.checkBoxLowbieUseWrath.TabIndex = 53;
            this.checkBoxLowbieUseWrath.Text = "Use Wrath";
            this.checkBoxLowbieUseWrath.UseVisualStyleBackColor = true;
            // 
            // radioButtonBalanceRotation
            // 
            this.radioButtonBalanceRotation.AutoSize = true;
            this.radioButtonBalanceRotation.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.radioButtonBalanceRotation.Location = new System.Drawing.Point(6, 23);
            this.radioButtonBalanceRotation.Name = "radioButtonBalanceRotation";
            this.radioButtonBalanceRotation.Size = new System.Drawing.Size(190, 22);
            this.radioButtonBalanceRotation.TabIndex = 52;
            this.radioButtonBalanceRotation.TabStop = true;
            this.radioButtonBalanceRotation.Text = "Use Balance Rotation";
            this.radioButtonBalanceRotation.UseVisualStyleBackColor = true;
            this.radioButtonBalanceRotation.Click += new System.EventHandler(this.radioButtonBalanceRotation_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label2);
            this.groupBox5.Controls.Add(this.radioButtonCatRotation);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.groupBox5.Location = new System.Drawing.Point(217, 35);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(206, 234);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Cat Settings";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Gray;
            this.label2.Location = new System.Drawing.Point(32, 128);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 16);
            this.label2.TabIndex = 52;
            this.label2.Text = "More Coming Soon";
            // 
            // radioButtonCatRotation
            // 
            this.radioButtonCatRotation.AutoSize = true;
            this.radioButtonCatRotation.Enabled = false;
            this.radioButtonCatRotation.ForeColor = System.Drawing.Color.Gray;
            this.radioButtonCatRotation.Location = new System.Drawing.Point(12, 23);
            this.radioButtonCatRotation.Name = "radioButtonCatRotation";
            this.radioButtonCatRotation.Size = new System.Drawing.Size(156, 22);
            this.radioButtonCatRotation.TabIndex = 51;
            this.radioButtonCatRotation.TabStop = true;
            this.radioButtonCatRotation.Text = "Use Cat Rotation";
            this.radioButtonCatRotation.UseVisualStyleBackColor = true;
            this.radioButtonCatRotation.Click += new System.EventHandler(this.radioButtonCatRotation_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.button1);
            this.groupBox6.Controls.Add(this.checkBoxUseHealingTouch);
            this.groupBox6.Controls.Add(this.checkBoxUseRegrowth);
            this.groupBox6.Controls.Add(this.checkBoxUseRejuvination);
            this.groupBox6.Controls.Add(this.label11);
            this.groupBox6.Controls.Add(this.numericUpDownHealHealingTouchHP);
            this.groupBox6.Controls.Add(this.label6);
            this.groupBox6.Controls.Add(this.numericUpDownRegrowHP);
            this.groupBox6.Controls.Add(this.label4);
            this.groupBox6.Controls.Add(this.numericUpDownHealRejivHp);
            this.groupBox6.Controls.Add(this.checkBoxHealCancelShapeshift);
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.groupBox6.Location = new System.Drawing.Point(5, 209);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(206, 197);
            this.groupBox6.TabIndex = 5;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Healing Settings";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.button1.Location = new System.Drawing.Point(9, 141);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(184, 44);
            this.button1.TabIndex = 70;
            this.button1.Text = "Group Heal Settings\r\n-Coming Soon-";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // checkBoxUseHealingTouch
            // 
            this.checkBoxUseHealingTouch.AutoSize = true;
            this.checkBoxUseHealingTouch.Checked = true;
            this.checkBoxUseHealingTouch.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxUseHealingTouch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxUseHealingTouch.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBoxUseHealingTouch.Location = new System.Drawing.Point(9, 112);
            this.checkBoxUseHealingTouch.Name = "checkBoxUseHealingTouch";
            this.checkBoxUseHealingTouch.Size = new System.Drawing.Size(15, 14);
            this.checkBoxUseHealingTouch.TabIndex = 69;
            this.checkBoxUseHealingTouch.UseVisualStyleBackColor = true;
            // 
            // checkBoxUseRegrowth
            // 
            this.checkBoxUseRegrowth.AutoSize = true;
            this.checkBoxUseRegrowth.Checked = true;
            this.checkBoxUseRegrowth.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxUseRegrowth.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxUseRegrowth.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBoxUseRegrowth.Location = new System.Drawing.Point(9, 87);
            this.checkBoxUseRegrowth.Name = "checkBoxUseRegrowth";
            this.checkBoxUseRegrowth.Size = new System.Drawing.Size(15, 14);
            this.checkBoxUseRegrowth.TabIndex = 68;
            this.checkBoxUseRegrowth.UseVisualStyleBackColor = true;
            // 
            // checkBoxUseRejuvination
            // 
            this.checkBoxUseRejuvination.AutoSize = true;
            this.checkBoxUseRejuvination.Checked = true;
            this.checkBoxUseRejuvination.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxUseRejuvination.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxUseRejuvination.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBoxUseRejuvination.Location = new System.Drawing.Point(9, 60);
            this.checkBoxUseRejuvination.Name = "checkBoxUseRejuvination";
            this.checkBoxUseRejuvination.Size = new System.Drawing.Size(15, 14);
            this.checkBoxUseRejuvination.TabIndex = 67;
            this.checkBoxUseRejuvination.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label11.Location = new System.Drawing.Point(28, 110);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(120, 16);
            this.label11.TabIndex = 66;
            this.label11.Text = "Big Heal if HP% <=";
            // 
            // numericUpDownHealHealingTouchHP
            // 
            this.numericUpDownHealHealingTouchHP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownHealHealingTouchHP.Location = new System.Drawing.Point(154, 108);
            this.numericUpDownHealHealingTouchHP.Maximum = new decimal(new int[] {
            85,
            0,
            0,
            0});
            this.numericUpDownHealHealingTouchHP.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownHealHealingTouchHP.Name = "numericUpDownHealHealingTouchHP";
            this.numericUpDownHealHealingTouchHP.Size = new System.Drawing.Size(42, 22);
            this.numericUpDownHealHealingTouchHP.TabIndex = 65;
            this.numericUpDownHealHealingTouchHP.Value = new decimal(new int[] {
            35,
            0,
            0,
            0});
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label6.Location = new System.Drawing.Point(28, 85);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(115, 16);
            this.label6.TabIndex = 64;
            this.label6.Text = "Regrow if HP% <=";
            // 
            // numericUpDownRegrowHP
            // 
            this.numericUpDownRegrowHP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownRegrowHP.Location = new System.Drawing.Point(149, 83);
            this.numericUpDownRegrowHP.Maximum = new decimal(new int[] {
            85,
            0,
            0,
            0});
            this.numericUpDownRegrowHP.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownRegrowHP.Name = "numericUpDownRegrowHP";
            this.numericUpDownRegrowHP.Size = new System.Drawing.Size(42, 22);
            this.numericUpDownRegrowHP.TabIndex = 63;
            this.numericUpDownRegrowHP.Value = new decimal(new int[] {
            45,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label4.Location = new System.Drawing.Point(28, 60);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(103, 16);
            this.label4.TabIndex = 62;
            this.label4.Text = "Rejuv if HP% <=";
            // 
            // numericUpDownHealRejivHp
            // 
            this.numericUpDownHealRejivHp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownHealRejivHp.Location = new System.Drawing.Point(137, 58);
            this.numericUpDownHealRejivHp.Maximum = new decimal(new int[] {
            85,
            0,
            0,
            0});
            this.numericUpDownHealRejivHp.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownHealRejivHp.Name = "numericUpDownHealRejivHp";
            this.numericUpDownHealRejivHp.Size = new System.Drawing.Size(42, 22);
            this.numericUpDownHealRejivHp.TabIndex = 61;
            this.numericUpDownHealRejivHp.Value = new decimal(new int[] {
            65,
            0,
            0,
            0});
            // 
            // checkBoxHealCancelShapeshift
            // 
            this.checkBoxHealCancelShapeshift.AutoSize = true;
            this.checkBoxHealCancelShapeshift.Checked = true;
            this.checkBoxHealCancelShapeshift.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxHealCancelShapeshift.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxHealCancelShapeshift.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBoxHealCancelShapeshift.Location = new System.Drawing.Point(9, 23);
            this.checkBoxHealCancelShapeshift.Name = "checkBoxHealCancelShapeshift";
            this.checkBoxHealCancelShapeshift.Size = new System.Drawing.Size(181, 20);
            this.checkBoxHealCancelShapeshift.TabIndex = 42;
            this.checkBoxHealCancelShapeshift.Text = "Cancel Shapeshift to Heal";
            this.checkBoxHealCancelShapeshift.UseVisualStyleBackColor = true;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.linkLabelEmu);
            this.groupBox7.Controls.Add(this.linkLabelZzuk);
            this.groupBox7.Controls.Add(this.buttonAboutForumThread);
            this.groupBox7.Controls.Add(this.label21);
            this.groupBox7.Controls.Add(this.label18);
            this.groupBox7.Controls.Add(this.linkLabelSeeSharp);
            this.groupBox7.Controls.Add(this.labelCCVersionName);
            this.groupBox7.Controls.Add(this.pictureBox3);
            this.groupBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.groupBox7.Location = new System.Drawing.Point(641, 35);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(206, 304);
            this.groupBox7.TabIndex = 5;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "About";
            this.groupBox7.Enter += new System.EventHandler(this.groupBox7_Enter);
            // 
            // linkLabelEmu
            // 
            this.linkLabelEmu.AutoSize = true;
            this.linkLabelEmu.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabelEmu.LinkColor = System.Drawing.Color.White;
            this.linkLabelEmu.Location = new System.Drawing.Point(150, 239);
            this.linkLabelEmu.Name = "linkLabelEmu";
            this.linkLabelEmu.Size = new System.Drawing.Size(35, 16);
            this.linkLabelEmu.TabIndex = 45;
            this.linkLabelEmu.TabStop = true;
            this.linkLabelEmu.Text = "Emu";
            this.linkLabelEmu.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelEmu_LinkClicked);
            // 
            // linkLabelZzuk
            // 
            this.linkLabelZzuk.AutoSize = true;
            this.linkLabelZzuk.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabelZzuk.LinkColor = System.Drawing.Color.White;
            this.linkLabelZzuk.Location = new System.Drawing.Point(112, 239);
            this.linkLabelZzuk.Name = "linkLabelZzuk";
            this.linkLabelZzuk.Size = new System.Drawing.Size(36, 16);
            this.linkLabelZzuk.TabIndex = 44;
            this.linkLabelZzuk.TabStop = true;
            this.linkLabelZzuk.Text = "Zzuk";
            this.linkLabelZzuk.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelZzuk_LinkClicked);
            // 
            // buttonAboutForumThread
            // 
            this.buttonAboutForumThread.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAboutForumThread.ForeColor = System.Drawing.Color.Black;
            this.buttonAboutForumThread.Location = new System.Drawing.Point(26, 262);
            this.buttonAboutForumThread.Name = "buttonAboutForumThread";
            this.buttonAboutForumThread.Size = new System.Drawing.Size(150, 26);
            this.buttonAboutForumThread.TabIndex = 43;
            this.buttonAboutForumThread.Text = "Forum Thread";
            this.buttonAboutForumThread.UseVisualStyleBackColor = true;
            this.buttonAboutForumThread.Click += new System.EventHandler(this.buttonAboutForumThread_Click);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.label21.Location = new System.Drawing.Point(7, 239);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(105, 16);
            this.label21.TabIndex = 42;
            this.label21.Text = "Special Thanks:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.label18.Location = new System.Drawing.Point(7, 215);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(96, 18);
            this.label18.TabIndex = 40;
            this.label18.Text = "Created By:";
            // 
            // linkLabelSeeSharp
            // 
            this.linkLabelSeeSharp.AutoSize = true;
            this.linkLabelSeeSharp.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabelSeeSharp.LinkColor = System.Drawing.Color.White;
            this.linkLabelSeeSharp.Location = new System.Drawing.Point(101, 214);
            this.linkLabelSeeSharp.Name = "linkLabelSeeSharp";
            this.linkLabelSeeSharp.Size = new System.Drawing.Size(89, 20);
            this.linkLabelSeeSharp.TabIndex = 41;
            this.linkLabelSeeSharp.TabStop = true;
            this.linkLabelSeeSharp.Text = "SeeSharp";
            this.linkLabelSeeSharp.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelSeeSharp_LinkClicked);
            // 
            // labelCCVersionName
            // 
            this.labelCCVersionName.AutoSize = true;
            this.labelCCVersionName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCCVersionName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.labelCCVersionName.Location = new System.Drawing.Point(26, 184);
            this.labelCCVersionName.Name = "labelCCVersionName";
            this.labelCCVersionName.Size = new System.Drawing.Size(122, 16);
            this.labelCCVersionName.TabIndex = 1;
            this.labelCCVersionName.Text = "VERSION NAME";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::DruidCCv2Dev.Properties.Resources.zDruidlogo;
            this.pictureBox3.Location = new System.Drawing.Point(26, 26);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(155, 155);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 0;
            this.pictureBox3.TabStop = false;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.label8);
            this.groupBox8.Controls.Add(this.checkBoxBuffThorns);
            this.groupBox8.Controls.Add(this.checkBoxBuffMarkOfTheWild);
            this.groupBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.groupBox8.Location = new System.Drawing.Point(429, 316);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(206, 118);
            this.groupBox8.TabIndex = 6;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Buff Settings";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Gray;
            this.label8.Location = new System.Drawing.Point(33, 87);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(123, 16);
            this.label8.TabIndex = 53;
            this.label8.Text = "More Coming Soon";
            // 
            // checkBoxBuffThorns
            // 
            this.checkBoxBuffThorns.AutoSize = true;
            this.checkBoxBuffThorns.Checked = true;
            this.checkBoxBuffThorns.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxBuffThorns.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxBuffThorns.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBoxBuffThorns.Location = new System.Drawing.Point(6, 52);
            this.checkBoxBuffThorns.Name = "checkBoxBuffThorns";
            this.checkBoxBuffThorns.Size = new System.Drawing.Size(69, 20);
            this.checkBoxBuffThorns.TabIndex = 46;
            this.checkBoxBuffThorns.Text = "Thorns";
            this.checkBoxBuffThorns.UseVisualStyleBackColor = true;
            // 
            // checkBoxBuffMarkOfTheWild
            // 
            this.checkBoxBuffMarkOfTheWild.AutoSize = true;
            this.checkBoxBuffMarkOfTheWild.Checked = true;
            this.checkBoxBuffMarkOfTheWild.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxBuffMarkOfTheWild.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxBuffMarkOfTheWild.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBoxBuffMarkOfTheWild.Location = new System.Drawing.Point(6, 26);
            this.checkBoxBuffMarkOfTheWild.Name = "checkBoxBuffMarkOfTheWild";
            this.checkBoxBuffMarkOfTheWild.Size = new System.Drawing.Size(122, 20);
            this.checkBoxBuffMarkOfTheWild.TabIndex = 45;
            this.checkBoxBuffMarkOfTheWild.Text = "Mark of the Wild";
            this.checkBoxBuffMarkOfTheWild.UseVisualStyleBackColor = true;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.label23);
            this.groupBox9.Controls.Add(this.buttonWelcomeMessageShow);
            this.groupBox9.Controls.Add(this.button2);
            this.groupBox9.Controls.Add(this.buttonLoadDefaultSettings);
            this.groupBox9.Controls.Add(this.comboBox1);
            this.groupBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.groupBox9.Location = new System.Drawing.Point(641, 345);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(206, 195);
            this.groupBox9.TabIndex = 7;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Misc";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Gray;
            this.label23.Location = new System.Drawing.Point(41, 113);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(123, 16);
            this.label23.TabIndex = 51;
            this.label23.Text = "More Coming Soon";
            // 
            // buttonWelcomeMessageShow
            // 
            this.buttonWelcomeMessageShow.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonWelcomeMessageShow.ForeColor = System.Drawing.Color.Black;
            this.buttonWelcomeMessageShow.Location = new System.Drawing.Point(10, 156);
            this.buttonWelcomeMessageShow.Name = "buttonWelcomeMessageShow";
            this.buttonWelcomeMessageShow.Size = new System.Drawing.Size(187, 26);
            this.buttonWelcomeMessageShow.TabIndex = 50;
            this.buttonWelcomeMessageShow.Text = "Show Welcome";
            this.buttonWelcomeMessageShow.UseVisualStyleBackColor = true;
            this.buttonWelcomeMessageShow.Click += new System.EventHandler(this.buttonWelcomeMessageShow_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.button2.Location = new System.Drawing.Point(7, 23);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(187, 26);
            this.button2.TabIndex = 49;
            this.button2.Text = "Found a Bug?";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // buttonLoadDefaultSettings
            // 
            this.buttonLoadDefaultSettings.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonLoadDefaultSettings.ForeColor = System.Drawing.Color.Black;
            this.buttonLoadDefaultSettings.Location = new System.Drawing.Point(132, 58);
            this.buttonLoadDefaultSettings.Name = "buttonLoadDefaultSettings";
            this.buttonLoadDefaultSettings.Size = new System.Drawing.Size(62, 25);
            this.buttonLoadDefaultSettings.TabIndex = 48;
            this.buttonLoadDefaultSettings.Text = "< Load";
            this.buttonLoadDefaultSettings.UseVisualStyleBackColor = true;
            this.buttonLoadDefaultSettings.Click += new System.EventHandler(this.buttonLoadDefaultSettings_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(7, 59);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 24);
            this.comboBox1.TabIndex = 47;
            this.comboBox1.Text = "Load a Default";
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.checkBox2);
            this.groupBox10.Controls.Add(this.label10);
            this.groupBox10.Controls.Add(this.numericUpDown1);
            this.groupBox10.Controls.Add(this.numericUpDown2);
            this.groupBox10.Controls.Add(this.checkBox3);
            this.groupBox10.Controls.Add(this.label13);
            this.groupBox10.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.groupBox10.Location = new System.Drawing.Point(429, 434);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(206, 106);
            this.groupBox10.TabIndex = 8;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Humanizer (NYI)";
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBox2.Location = new System.Drawing.Point(12, 50);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(93, 20);
            this.checkBox2.TabIndex = 57;
            this.checkBox2.Text = "Cast Delay";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(162, 78);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(28, 16);
            this.label10.TabIndex = 61;
            this.label10.Text = "MS";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown1.Location = new System.Drawing.Point(96, 73);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(60, 22);
            this.numericUpDown1.TabIndex = 59;
            this.numericUpDown1.Value = new decimal(new int[] {
            250,
            0,
            0,
            0});
            // 
            // numericUpDown2
            // 
            this.numericUpDown2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown2.Location = new System.Drawing.Point(12, 73);
            this.numericUpDown2.Name = "numericUpDown2";
            this.numericUpDown2.Size = new System.Drawing.Size(57, 22);
            this.numericUpDown2.TabIndex = 58;
            this.numericUpDown2.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBox3.Location = new System.Drawing.Point(12, 24);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(146, 20);
            this.checkBox3.TabIndex = 56;
            this.checkBox3.Text = "Make Human Errors";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(75, 74);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(14, 18);
            this.label13.TabIndex = 60;
            this.label13.Text = "-";
            // 
            // CCGui
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.ClientSize = new System.Drawing.Size(854, 591);
            this.Controls.Add(this.groupBox10);
            this.Controls.Add(this.groupBox9);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CCGui";
            this.Text = "CCGui";
            this.Load += new System.EventHandler(this.CCGui_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRestHealPercent)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownNoPullIfTargerLevelGreater)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownNoPullIfTargetLevelLess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPullRange)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSwipeMinTargets)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBearHealHPPercent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDemoRoarMinTargets)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHealHealingTouchHP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRegrowHP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHealRejivHp)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label labelCCCuiTitle;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button buttonSaveAndCloseGUI;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label labelCCVersionName;
        private System.Windows.Forms.LinkLabel linkLabelEmu;
        private System.Windows.Forms.LinkLabel linkLabelZzuk;
        private System.Windows.Forms.Button buttonAboutForumThread;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.LinkLabel linkLabelSeeSharp;
        private System.Windows.Forms.NumericUpDown numericUpDownRestHealPercent;
        private System.Windows.Forms.CheckBox checkBoxRestUseHeal;
        private System.Windows.Forms.TextBox textBoxRestDrinkname;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxRestFoodname;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxPullingSpell;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown numericUpDownPullRange;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.CheckBox checkBoxNoPullIfTargetSkull;
        private System.Windows.Forms.NumericUpDown numericUpDownNoPullIfTargerLevelGreater;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.NumericUpDown numericUpDownNoPullIfTargetLevelLess;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.CheckBox checkBoxPullWithShapeshiftAuto;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.NumericUpDown numericUpDownDemoRoarMinTargets;
        private System.Windows.Forms.CheckBox checkBoxBearUseSwipe;
        private System.Windows.Forms.CheckBox checkBoxBearUseDemoRoar;
        private System.Windows.Forms.CheckBox checkBoxBearUseMaul;
        private System.Windows.Forms.RadioButton radioButtonBearRotation;
        private System.Windows.Forms.CheckBox checkBoxBearUseBash;
        private System.Windows.Forms.CheckBox checkBoxBearUseEnrage;
        private System.Windows.Forms.RadioButton radioButtonCatRotation;
        private System.Windows.Forms.RadioButton radioButtonBalanceRotation;
        private System.Windows.Forms.CheckBox checkBoxLowbieUseWrath;
        private System.Windows.Forms.CheckBox checkBoxLowbieUseMoonfire;
        private System.Windows.Forms.CheckBox checkBoxBuffThorns;
        private System.Windows.Forms.CheckBox checkBoxBuffMarkOfTheWild;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Button buttonLoadDefaultSettings;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.NumericUpDown numericUpDownBearHealHPPercent;
        private System.Windows.Forms.CheckBox checkBoxHealCancelShapeshift;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown numericUpDownRegrowHP;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown numericUpDownHealRejivHp;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.NumericUpDown numericUpDownHealHealingTouchHP;
        private System.Windows.Forms.CheckBox checkBoxUseHealingTouch;
        private System.Windows.Forms.CheckBox checkBoxUseRegrowth;
        private System.Windows.Forms.CheckBox checkBoxUseRejuvination;
        private System.Windows.Forms.NumericUpDown numericUpDownSwipeMinTargets;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.NumericUpDown numericUpDown2;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button buttonWelcomeMessageShow;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label8;
    }
}