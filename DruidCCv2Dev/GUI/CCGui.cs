﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using DruidCCv2Dev;
using DruidCCv2Dev.Settings;
using DruidCCv2Dev.MoreMethods;
using ZzukBot.Settings;
using DruidCCv2Dev.Constants;

namespace DruidCCv2Dev.GUI
{
    public partial class CCGui : Form
    {
        public zDruid parentCCObj;
        public zDruidSettings settingsGui;

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        public CCGui()
        {
            InitializeComponent();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Helpers.PrintToChat("GUI Closing without saving");
            this.Close();
        }

        private void buttonSaveAndCloseGUI_Click(object sender, EventArgs e)
        {
            try
            {
                SaveSettings();
                SaveJSON();
                Helpers.PrintToChat("GUI Closing");
            }
            catch (Exception exc)
            {
                MessageBox.Show("ERROR SAVING SETTINGS" + Environment.NewLine + exc.GetType().ToString());
                Helpers.PrintToChat("GUI Closing without saving");
            }
            this.Close();
        }

        private void linkLabelSeeSharp_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://zzukbot.com/forum/memberlist.php?mode=viewprofile&u=1675");
        }

        private void linkLabelZzuk_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://zzukbot.com/forum/memberlist.php?mode=viewprofile&u=58");
        }

        private void linkLabelEmu_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://zzukbot.com/forum/memberlist.php?mode=viewprofile&u=2");
        }

        private void buttonAboutForumThread_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://zzukbot.com/forum/viewtopic.php?f=61&t=1151");
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void CCGui_Load(object sender, EventArgs e)
        {
            labelCCCuiTitle.Text = zDruidConstants.ccName + zDruidConstants.ccGUITitle + " V:" +zDruidConstants.ccVersion;
            labelCCVersionName.Text = "Version: " + zDruidConstants.ccVersion + " Alpha";
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void buttonLoadDefaultSettings_Click(object sender, EventArgs e)
        {

        }

        public void LoadSettings()
        {
            if (settingsGui.fightStyle == Constants.zDruidEnums.DruidFightStyleEnum.Lowbie)
            {
                radioButtonBalanceRotation.Checked = true;

                radioButtonBearRotation.Checked = false;
                radioButtonCatRotation.Checked = false;
            }
            else if (settingsGui.fightStyle == Constants.zDruidEnums.DruidFightStyleEnum.Bear)
            {
                radioButtonBearRotation.Checked = true;

                radioButtonBalanceRotation.Checked = false;
                radioButtonCatRotation.Checked = false;
            }
            else if (settingsGui.fightStyle == Constants.zDruidEnums.DruidFightStyleEnum.Cat)
            {
                radioButtonCatRotation.Checked = true;

                radioButtonBearRotation.Checked = false;
                radioButtonBalanceRotation.Checked = false;
            }

            //load buff settins
            checkBoxBuffMarkOfTheWild.Checked = settingsGui.buffMarkOfTheWild;
            checkBoxBuffThorns.Checked = settingsGui.buffThorns;

            //load rest settings
            textBoxRestFoodname.Text = settingsGui.restFood;
            textBoxRestDrinkname.Text = settingsGui.restDrink;
            checkBoxRestUseHeal.Checked = settingsGui.restUseHeal;
            numericUpDownRestHealPercent.Value = settingsGui.restHealHP;

            //load pull settings
            numericUpDownPullRange.Value = settingsGui.pullDistance;
            textBoxPullingSpell.Text = settingsGui.pullSpell;
            checkBoxPullWithShapeshiftAuto.Checked = settingsGui.pullShapeShiftAA;

            //load heal settings
            checkBoxHealCancelShapeshift.Checked = settingsGui.cancelShapeshiftsToHeal;

            numericUpDownHealRejivHp.Value = settingsGui.healRejuvinationPercentHP;
            numericUpDownRegrowHP.Value = settingsGui.healRegrowthPercentHP;
            numericUpDownHealHealingTouchHP.Value = settingsGui.healHealingTouchPercentHP;
            checkBoxUseRejuvination.Checked = settingsGui.useRejuvinationHeal;
            checkBoxUseRegrowth.Checked = settingsGui.useRegrowthHeal;
            checkBoxUseHealingTouch.Checked = settingsGui.useHealingTouchHeal;

            //load lowbie settings
            checkBoxLowbieUseWrath.Checked = settingsGui.useWrath;
            checkBoxLowbieUseMoonfire.Checked = settingsGui.useMoonfire;

            //load bear settings
            checkBoxBearUseDemoRoar.Checked = settingsGui.useDemoralizingRoar;
            numericUpDownDemoRoarMinTargets.Value = settingsGui.demoRoarMinTargetNum;
            checkBoxBearUseSwipe.Checked = settingsGui.useSwipeAOE;
            numericUpDownSwipeMinTargets.Value = settingsGui.swipeMinTargetNum;
            checkBoxBearUseEnrage.Checked = settingsGui.useBearEnrage;
            checkBoxBearUseBash.Checked = settingsGui.useBearBash;
            checkBoxBearUseMaul.Checked = settingsGui.useBearMaulFiller;

            numericUpDownBearHealHPPercent.Value = settingsGui.bearHealPercentHP;

            Helpers.PrintToChat("GUI Settings Loaded");

        }
        public void SaveSettings()
        {
            if (radioButtonBalanceRotation.Checked)
            {
                settingsGui.fightStyle = Constants.zDruidEnums.DruidFightStyleEnum.Lowbie;
            }
            else if (radioButtonBearRotation.Checked)
            {
                settingsGui.fightStyle = Constants.zDruidEnums.DruidFightStyleEnum.Bear;
            }
            else if (radioButtonCatRotation.Checked)
            {
                settingsGui.fightStyle = Constants.zDruidEnums.DruidFightStyleEnum.Cat;
            }

            //load buff settins
            settingsGui.buffMarkOfTheWild = checkBoxBuffMarkOfTheWild.Checked;
            settingsGui.buffThorns = checkBoxBuffThorns.Checked;

            //load rest settings
            settingsGui.restFood = textBoxRestFoodname.Text;
            settingsGui.restDrink = textBoxRestDrinkname.Text;
            settingsGui.restUseHeal = checkBoxRestUseHeal.Checked;
            settingsGui.restHealHP = numericUpDownRestHealPercent.Value;

            //load pull settings
            settingsGui.pullDistance = numericUpDownPullRange.Value;
            settingsGui.pullSpell = textBoxPullingSpell.Text;
            settingsGui.pullShapeShiftAA = checkBoxPullWithShapeshiftAuto.Checked;

            //load heal settings
            settingsGui.cancelShapeshiftsToHeal = checkBoxHealCancelShapeshift.Checked;

            settingsGui.healRejuvinationPercentHP = numericUpDownHealRejivHp.Value;
            settingsGui.healRegrowthPercentHP = numericUpDownRegrowHP.Value;
            settingsGui.healHealingTouchPercentHP = numericUpDownHealHealingTouchHP.Value;
            settingsGui.useRejuvinationHeal = checkBoxUseRejuvination.Checked;
            settingsGui.useRegrowthHeal = checkBoxUseRegrowth.Checked;
            settingsGui.useHealingTouchHeal = checkBoxUseHealingTouch.Checked;

            //load lowbie settings
            settingsGui.useWrath = checkBoxLowbieUseWrath.Checked;
            settingsGui.useMoonfire = checkBoxLowbieUseMoonfire.Checked;

            //load bear settings
            settingsGui.useDemoralizingRoar = checkBoxBearUseDemoRoar.Checked;
            settingsGui.demoRoarMinTargetNum = numericUpDownDemoRoarMinTargets.Value;
            settingsGui.useSwipeAOE = checkBoxBearUseSwipe.Checked;
            settingsGui.swipeMinTargetNum = numericUpDownSwipeMinTargets.Value;
            settingsGui.useBearEnrage = checkBoxBearUseEnrage.Checked;
            settingsGui.useBearBash = checkBoxBearUseBash.Checked;
            settingsGui.useBearMaulFiller = checkBoxBearUseMaul.Checked;

            settingsGui.bearHealPercentHP = numericUpDownBearHealHPPercent.Value;

            Helpers.PrintToChat("GUI Settings Saved to local Session");

        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Coming Soon!");
        }

        private void buttonWelcomeMessageShow_Click(object sender, EventArgs e)
        {
            parentCCObj.welcomeForm.Show();
        }

        private void SaveJSON()
        {
            try
            {
                OptionManager.Get(zDruidConstants.ccName).SaveToJson(settingsGui);
                Helpers.PrintToChat("GUI Settings Saved to JSON");
            }
            catch
            {
                Helpers.PrintToChat("Error Saving to JSON!");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://zzukbot.com/forum/viewtopic.php?f=61&t=1151");
        }

        private void radioButtonBearRotation_Click(object sender, EventArgs e)
        {
            radioButtonCatRotation.Checked = false;
            radioButtonBearRotation.Checked = true;
            radioButtonBalanceRotation.Checked = false;
        }

        private void radioButtonCatRotation_Click(object sender, EventArgs e)
        {
            radioButtonCatRotation.Checked = true;
            radioButtonBearRotation.Checked = false;
            radioButtonBalanceRotation.Checked = false;
        }

        private void radioButtonBalanceRotation_Click(object sender, EventArgs e)
        {
            radioButtonCatRotation.Checked = false;
            radioButtonBearRotation.Checked = false;
            radioButtonBalanceRotation.Checked = true;
        }

        private void groupBox7_Enter(object sender, EventArgs e)
        {

        }
    }
}
