﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DruidCCv2Dev.Constants;
using DruidCCv2Dev.MoreMethods;

namespace DruidCCv2Dev.Settings
{
    public class zDruidSettings
    {
        //buff settings
        public bool buffMarkOfTheWild = true;
        public bool buffThorns = true;

        //balance settings
        public bool useMoonfire = true;
        public bool useWrath = true;

        //bear setings
        public bool useDemoralizingRoar = true;
        public decimal demoRoarMinTargetNum = 1;

        public bool useSwipeAOE = true;
        public decimal swipeMinTargetNum = 2;

        public bool useBearEnrage = true;

        public bool useBearBash = true;

        public bool useBearMaulFiller = true;

        public decimal bearHealPercentHP = 40;

        //heal settings
        public bool cancelShapeshiftsToHeal = true;

        public bool useRejuvinationHeal = true;
        public bool useRegrowthHeal = true;
        public bool useHealingTouchHeal = true;

        public decimal healRejuvinationPercentHP = 65;
        public decimal healRegrowthPercentHP = 45;
        public decimal healHealingTouchPercentHP = 35;

        //pull settigns
        public string pullSpell = "Wrath";
        public decimal pullDistance = 25;

        public bool pullShapeShiftAA = false;

        public zDruidEnums.DruidFightStyleEnum fightStyle = zDruidEnums.DruidFightStyleEnum.Lowbie;

        //Rest Settings
        public string restFood = "SET ME";
        public string restDrink = "SET ME";
        public bool restUseHeal = false;
        public decimal restHealHP = 35;


        public void SetDefaults()
        {
            //buff settings
            buffMarkOfTheWild = true;
            buffThorns = true;

                    //balance settings
            useMoonfire = true;
            useWrath = true;

                    //bear setings
            useDemoralizingRoar = true;
            demoRoarMinTargetNum = 1;

            useSwipeAOE = true;
            swipeMinTargetNum = 2;

            useBearEnrage = true;

            useBearBash = true;

            useBearMaulFiller = true;

            bearHealPercentHP = 40;

                    //heal settings
            cancelShapeshiftsToHeal = true;

            useRejuvinationHeal = true;
            useRegrowthHeal = true;
            useHealingTouchHeal = true;

            healRejuvinationPercentHP = 65;
            healRegrowthPercentHP = 45;
            healHealingTouchPercentHP = 35;

                    //pull settigns
            pullSpell = "Wrath";
            pullDistance = 25;

            fightStyle = zDruidEnums.DruidFightStyleEnum.Lowbie;
            Helpers.PrintToChat("Loaded Default Settings Preset");
    }

}
}
