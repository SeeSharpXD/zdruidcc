﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;

using ZzukBot.ExtensionFramework;
using ZzukBot.ExtensionFramework.Classes;
using ZzukBot.Game.Statics;
using ZzukBot.Objects;
using ZzukBot.Settings;
using ZzukBot.Constants;

using DruidCCv2Dev.MoreMethods;
using DruidCCv2Dev.Constants;
using DruidCCv2Dev.Settings;
using DruidCCv2Dev.GUI;

namespace DruidCCv2Dev
{
    [Export(typeof(CustomClass))]
    public class zDruid : CustomClass
    {
        public CCGui guiFormObject = new CCGui();   
        public zDruidSettings settingsCC = new zDruidSettings();
        public WelcomeForm welcomeForm = new WelcomeForm();

        public override string Author => "SeeSharp";
        public override Enums.ClassId Class => Enums.ClassId.Druid;//This is in ZZukBot.Constants
        //override float CombatDistance
        //public override float CombatDistance { get { return 26.5f; } }
        public override string Name => zDruidConstants.ccName + " " + zDruidConstants.ccVersion + " Alpha";

        bool healToShapeshift = false;

        public override bool Load()
        {//This should be called anytime the custom class loads, do things like set settings from a file here
            welcomeForm.labelVersionText.Text = zDruidConstants.ccVersion + " - Alpha";
            welcomeForm.parentCCObj = this;
            try
            {
                settingsCC = OptionManager.Get(zDruidConstants.ccName).LoadFromJson<zDruidSettings>();
                Helpers.PrintToChat("CC Loaded");
            }
            catch (Exception ex)
            {
                settingsCC.SetDefaults();
                Helpers.PrintToChat("Error Loading settings " + ex.GetType().ToString());
            }
            try
            {
                //check for complete object
                if (settingsCC.buffMarkOfTheWild)
                {
                    settingsCC.buffMarkOfTheWild = settingsCC.buffMarkOfTheWild;
                }
            }
            catch(Exception ex)
            {
                welcomeForm.Show();
                settingsCC = new zDruidSettings();
                settingsCC.SetDefaults();
                OptionManager.Get(zDruidConstants.ccName).SaveToJson(settingsCC);
                Helpers.PrintToChat("JSON File Created!");
            }


            return true;
        }

        public override bool OnBuff()
        {//returns true when done buffing, this is called during the onRest functions
            try
            {
                if (ObjectManager.Instance.Player.Casting != 0 || ObjectManager.Instance.Player.Channeling != 0 || ObjectManager.Instance.Player.IsDead || ObjectManager.Instance.Player.InGhostForm)
                {
                    return false;
                }
                if (settingsCC.buffMarkOfTheWild && !ObjectManager.Instance.Player.GotAura("Mark of the Wild") && Helpers.CanCast("Mark of the Wild"))
                {
                    if (Spell.Instance.IsShapeShifted)
                    {
                        Spell.Instance.CancelShapeshift();
                        return false;
                    }
                    Spell.Instance.Cast("Mark of the Wild");
                    Helpers.PrintToChat("Buffing - Mark of the Wild - Rank " + Spell.Instance.GetSpellRank("Mark of the Wild").ToString());
                    ZzukBot.Helpers.Wait.For("zDruid Buffing", 1500);
                    return false;
                }
                if (settingsCC.buffThorns && !ObjectManager.Instance.Player.GotAura("Thorns") && Helpers.CanCast("Thorns"))
                {
                    if (Spell.Instance.IsShapeShifted)
                    {
                        Spell.Instance.CancelShapeshift();
                        return false;
                    }
                    Spell.Instance.Cast("Thorns");
                    Helpers.PrintToChat("Buffing - Thorns - Rank " + Spell.Instance.GetSpellRank("Thorns").ToString());
                    ZzukBot.Helpers.Wait.For("zDruid Buffing", 1500);
                    return false;
                }
            }
            catch (Exception ex)
            {
                welcomeForm.Show();
                settingsCC = new zDruidSettings();
                settingsCC.SetDefaults();
                OptionManager.Get(zDruidConstants.ccName).SaveToJson(settingsCC);
                Helpers.PrintToChat("JSON File Created!");
            }
            return true;
        }

        public override void OnPull()
        {//How we pull, make sure the target is not null
            WoWUnit tar;
            try
            {
                tar = ObjectManager.Instance.Target;
                healToShapeshift = false;
                if (tar == null)
                {
                    return;
                }
                if (ObjectManager.Instance.Player.Casting != 0 || ObjectManager.Instance.Player.Channeling != 0 || ObjectManager.Instance.Player.IsDead || ObjectManager.Instance.Player.InGhostForm)
                {
                    return;
                }
                if (tar.Reaction == Enums.UnitReaction.Friendly)
                {
                    Helpers.PrintToChat("Target is Friendly. Cannot Pull!");
                    return;
                }
                /*
                if (CombatDistance != (float)settingsCC.pullDistance)
                {
                    CombatDistance = (float)settingsCC.pullDistance;
                }
                */
                if (CustomClasses.Instance.Current.CombatDistance != (float)settingsCC.pullDistance)
                {
                    CustomClasses.Instance.Current.CombatDistance = (float)settingsCC.pullDistance;
                }
                switch (settingsCC.fightStyle)
                {
                    case zDruidEnums.DruidFightStyleEnum.Lowbie:
                        {
                            if (Spell.Instance.IsShapeShifted)
                            {
                                Spell.Instance.CancelShapeshift();
                            }
                            break;
                        }
                    case zDruidEnums.DruidFightStyleEnum.Bear:
                        {
                            if (!settingsCC.pullShapeShiftAA && Spell.Instance.IsShapeShifted)
                            {
                                Spell.Instance.CancelShapeshift();
                                return;
                            }
                            else if (settingsCC.pullShapeShiftAA && !Spell.Instance.IsShapeShifted)
                            {
                                Spell.Instance.CastWait("Bear Form", 2500);
                                return;
                            }
                            break;
                        }
                    case zDruidEnums.DruidFightStyleEnum.Cat:
                        {

                            break;
                        }

                }

                if ((!Helpers.CanCast(settingsCC.pullSpell) || settingsCC.pullSpell == "" || ObjectManager.Instance.Player.ManaPercent < 20) || (settingsCC.pullShapeShiftAA && settingsCC.fightStyle == zDruidEnums.DruidFightStyleEnum.Bear))
                {
                    if (CustomClasses.Instance.Current.CombatDistance != 3)
                    {
                        CustomClasses.Instance.Current.CombatDistance = 3;
                    }
                
                }
                if (tar.DistanceToPlayer > (float)settingsCC.pullDistance)
                {
                    return;
                }
                if (settingsCC.fightStyle == zDruidEnums.DruidFightStyleEnum.Bear && settingsCC.pullShapeShiftAA)
                {
                    Spell.Instance.Attack();
                    ZzukBot.Helpers.Wait.For("zDruid Pull", 250);
                    return;
                }
                if (Helpers.CanCast(settingsCC.pullSpell) && settingsCC.pullSpell != "" && ObjectManager.Instance.Player.ManaPercent >= 20)
                {
                    Spell.Instance.Cast(settingsCC.pullSpell);
                    Helpers.PrintToChat("Pulling with " + settingsCC.pullSpell);
                    ZzukBot.Helpers.Wait.For("zDruid Pull", 1000);
                    return;
                }

            }
            catch (Exception ex)
            {

            }

        }

        public override void OnFight()
        {//The combat routine AFTER pulling
            WoWUnit tar;
            try
            {
                tar = ObjectManager.Instance.Target;
                if (tar == null)
                {
                    return;
                }

                //casting and other returns
                if (ObjectManager.Instance.Player.Casting != 0 || ObjectManager.Instance.Player.Channeling != 0 || ObjectManager.Instance.Player.IsDead || ObjectManager.Instance.Player.InGhostForm)
                {
                    return;
                }

                Spell.Instance.Attack(); //auto attack

                switch (settingsCC.fightStyle)
                {
                    //lowbie
                    case zDruidEnums.DruidFightStyleEnum.Lowbie:
                        {
                            if (CustomClasses.Instance.Current.CombatDistance != (float)settingsCC.pullDistance)
                            {
                                CustomClasses.Instance.Current.CombatDistance = (float)settingsCC.pullDistance;
                            }
                            if (settingsCC.useHealingTouchHeal && ObjectManager.Instance.Player.HealthPercent <= settingsCC.healHealingTouchPercentHP && Helpers.CanCast("Healing Touch"))
                            {
                                Spell.Instance.Cast("Healing Touch");
                                return;
                            }
                            if (settingsCC.useRegrowthHeal && ObjectManager.Instance.Player.HealthPercent <= settingsCC.healRegrowthPercentHP && Helpers.CanCast("Regrowth"))
                            {
                                Spell.Instance.Cast("Regrowth");
                                return;
                            }
                            if (settingsCC.useRejuvinationHeal && ObjectManager.Instance.Player.HealthPercent <= settingsCC.healRejuvinationPercentHP && Helpers.CanCast("Rejuvenation") && !ObjectManager.Instance.Player.GotAura("Rejuvenation"))
                            {
                                Spell.Instance.Cast("Rejuvenation");
                                return;
                            }
                            if (settingsCC.useMoonfire && Helpers.CanCast("Moonfire") && !tar.GotDebuff("Moonfire") && tar.HealthPercent > 25 && ObjectManager.Instance.Player.ManaPercent >= 20)
                            {
                                Spell.Instance.CastWait("Moonfire", 5000);
                                return;
                            }

                            if (settingsCC.useWrath && Helpers.CanCast("Wrath") && ObjectManager.Instance.Player.ManaPercent >= 20)
                            {
                                Spell.Instance.Cast("Wrath");
                                return;
                            }
                            break;
                        }
                        //bear
                    case zDruidEnums.DruidFightStyleEnum.Bear:
                        {
                            if (!ObjectManager.Instance.Player.GotAura("Bear Form") || !Spell.Instance.IsShapeShifted)
                            {
                                if (healToShapeshift)
                                {
                                    if (ObjectManager.Instance.Player.HealthPercent >= settingsCC.bearHealPercentHP)
                                    {
                                        healToShapeshift = false;
                                        return;
                                    }
                                    if (settingsCC.useHealingTouchHeal && ObjectManager.Instance.Player.HealthPercent <= settingsCC.healHealingTouchPercentHP && Helpers.CanCast("Healing Touch"))
                                    {
                                        Spell.Instance.Cast("Healing Touch");
                                        return;
                                    }
                                    if (settingsCC.useRegrowthHeal && ObjectManager.Instance.Player.HealthPercent <= settingsCC.healRegrowthPercentHP && Helpers.CanCast("Regrowth"))
                                    {
                                        Spell.Instance.Cast("Regrowth");
                                        healToShapeshift = false;
                                        return;
                                    }
                                    if (settingsCC.useRejuvinationHeal && ObjectManager.Instance.Player.HealthPercent <= settingsCC.healRejuvinationPercentHP && Helpers.CanCast("Rejuvenation"))
                                    {
                                        Spell.Instance.Cast("Rejuvenation");
                                        healToShapeshift = false;
                                        return;
                                    }
                                }
                                else
                                {
                                    Spell.Instance.CastWait("Bear Form",2500);
                                }
                            }
                            else
                            {
                                if (CustomClasses.Instance.Current.CombatDistance != 3)
                                {
                                    CustomClasses.Instance.Current.CombatDistance = 3;
                                }
                                if (ObjectManager.Instance.Player.HealthPercent <= settingsCC.bearHealPercentHP && settingsCC.cancelShapeshiftsToHeal && ObjectManager.Instance.Player.ManaPercent > 30)
                                {
                                    Spell.Instance.CancelShapeshift();
                                    healToShapeshift = true;
                                    return;
                                }
                                if (settingsCC.useBearEnrage && Helpers.CanCast("Enrage") && tar.HealthPercent >= 60)
                                {
                                    Spell.Instance.Cast("Enrage");
                                    return;
                                }
                                if (settingsCC.useDemoralizingRoar && tar.HealthPercent > 50 && Helpers.CanCast("Demoralizing Roar") && UnitInfo.Instance.NpcAttackers.Count >= settingsCC.demoRoarMinTargetNum)
                                {
                                    Spell.Instance.Cast("Demoralizing Roar");
                                    return;
                                }
                                if (settingsCC.useSwipeAOE && Helpers.CanCast("Swipe") && UnitInfo.Instance.NpcAttackers.Count >= settingsCC.swipeMinTargetNum)
                                {
                                    Spell.Instance.Cast("Swipe");
                                    return;
                                }
                                if (settingsCC.useBearMaulFiller && Helpers.CanCast("Maul"))
                                {
                                    Spell.Instance.Cast("Maul");
                                    return;
                                }
                            }
                            break;
                        }
                }

            }
            catch (Exception ex)
            {

            }


        }

        public override void OnRest()
        {//What to do when told to rest (%mana and %health are low enough)
            try
            {
                if (ObjectManager.Instance.Player.Casting != 0 || ObjectManager.Instance.Player.Channeling != 0 || ObjectManager.Instance.Player.IsDead || ObjectManager.Instance.Player.InGhostForm)
                {
                    return;
                }

                if (settingsCC.restUseHeal && ObjectManager.Instance.Player.HealthPercent <= settingsCC.restHealHP)
                {
                    if (Spell.Instance.IsShapeShifted)
                    {
                        Spell.Instance.CancelShapeshift();
                        return;
                    }
                    if (settingsCC.useHealingTouchHeal && ObjectManager.Instance.Player.HealthPercent <= settingsCC.healHealingTouchPercentHP && Helpers.CanCast("Healing Touch"))
                    {
                        Spell.Instance.Cast("Healing Touch");
                        return;
                    }
                    if (settingsCC.useRegrowthHeal && ObjectManager.Instance.Player.HealthPercent <= settingsCC.healRegrowthPercentHP && Helpers.CanCast("Regrowth"))
                    {
                        Spell.Instance.Cast("Regrowth");
                        return;
                    }
                    if (settingsCC.useRejuvinationHeal && ObjectManager.Instance.Player.HealthPercent <= settingsCC.healRejuvinationPercentHP && Helpers.CanCast("Rejuvenation") && !ObjectManager.Instance.Player.GotAura("Rejuvenation"))
                    {
                        Spell.Instance.Cast("Rejuvenation");
                        return;
                    }
                }

                if (!ObjectManager.Instance.Player.IsDrinking && !ObjectManager.Instance.Player.GotAura("Drink") && Inventory.Instance.GetItemCount(settingsCC.restDrink) > 0 && ObjectManager.Instance.Player.ManaPercent < 85)
                {
                    if (Spell.Instance.IsShapeShifted)
                    {
                        Spell.Instance.CancelShapeshift();
                        return;
                    }
                    ObjectManager.Instance.Items.FirstOrDefault(i => i.Name == settingsCC.restDrink).Use();
                    //Helpers.PrintToChat("Drinking " + settingsCC.restDrink);
                    ZzukBot.Helpers.Wait.For("zDruid Drink", 750);
                    return;
                }
                if (!ObjectManager.Instance.Player.IsEating && !ObjectManager.Instance.Player.GotAura("Eat") && Inventory.Instance.GetItemCount(settingsCC.restFood) > 0 && ObjectManager.Instance.Player.HealthPercent < 85)
                {
                    if (Spell.Instance.IsShapeShifted)
                    {
                        Spell.Instance.CancelShapeshift();
                        return;
                    }
                    ObjectManager.Instance.Items.FirstOrDefault(i => i.Name == settingsCC.restFood).Use();
                    //Helpers.PrintToChat("Eating " + settingsCC.restFood);
                    ZzukBot.Helpers.Wait.For("zDruid Eat", 750);
                    return;
                }
            }
            catch (Exception ex)
            {
                Helpers.PrintToChat("ERROR RESTING");
            }
        }

        public override void ShowGui()
        {//Used to show the settings GUI if present. 

            guiFormObject.Dispose();
            guiFormObject = new CCGui();
            guiFormObject.parentCCObj = this;
            guiFormObject.settingsGui = settingsCC;
            try
            {
                guiFormObject.LoadSettings();
            }
            catch (Exception ex)
            {
                Helpers.PrintToChat("Error Loading GUI Settings. Trying fix!");
                settingsCC = new zDruidSettings();
                settingsCC.SetDefaults();
                OptionManager.Get(zDruidConstants.ccName).SaveToJson(settingsCC);
                Helpers.PrintToChat("New JSON File Created! Settings have been reset!");
                guiFormObject.settingsGui = settingsCC;
            }
            guiFormObject.Show();
            Helpers.PrintToChat("Opening GUI");
        }

        public override void Unload()
        {//Called when the CC is being unloaded
            try
            {
                Helpers.PrintToChat("Unloading!");
                OptionManager.Get(zDruidConstants.ccName).SaveToJson(settingsCC);
            }
            catch (Exception exc)
            {
                Helpers.PrintToChat("Unloading Error! " + exc.GetType().ToString());
            }
        }

        public override void _Dispose()
        {//Should be called when completely unloading the CC
            try
            {
                Helpers.PrintToChat("Disposing of CC memory!");
            }
            catch (Exception ex)
            {

            }
        }

    }
}